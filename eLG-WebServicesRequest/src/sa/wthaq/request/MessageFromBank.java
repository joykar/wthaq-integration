package sa.wthaq.request;

import java.util.Date;

public class MessageFromBank {
	
	private String request_Id;
	private String bankMessage_Id;
	private Date timeData;
	private String status_code;
	private String status_msg;
	private String valid_flag;
	private String messageToApplicant;
	public String getRequest_Id() {
		return request_Id;
	}
	public void setRequest_Id(String request_Id) {
		this.request_Id = request_Id;
	}
	public String getBankMessage_Id() {
		return bankMessage_Id;
	}
	public void setBankMessage_Id(String bankMessage_Id) {
		this.bankMessage_Id = bankMessage_Id;
	}
	public Date getTimeData() {
		return timeData;
	}
	public void setTimeData(Date timeData) {
		this.timeData = timeData;
	}
	public String getStatus_code() {
		return status_code;
	}
	public void setStatus_code(String status_code) {
		this.status_code = status_code;
	}
	public String getStatus_msg() {
		return status_msg;
	}
	public void setStatus_msg(String status_msg) {
		this.status_msg = status_msg;
	}
	public String getValid_flag() {
		return valid_flag;
	}
	public void setValid_flag(String valid_flag) {
		this.valid_flag = valid_flag;
	}
	public String getMessageToApplicant() {
		return messageToApplicant;
	}
	public void setMessageToApplicant(String messageToApplicant) {
		this.messageToApplicant = messageToApplicant;
	}
	
	

}
