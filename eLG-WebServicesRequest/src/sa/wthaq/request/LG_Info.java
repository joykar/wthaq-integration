package sa.wthaq.request;

import java.sql.Date;

public class LG_Info {
	private String LGType;
	private String projectName;
	private String projectNumber;
	private String byanNumber;
	private Date zakatPeriodStart;
	private Date zakatPeriodEndDate;
	private String purposeOfBbond;
	private Date LGValidity;
	private String hijriDate;
	private Long amount;
	private String amountText;
	private String percentageOfContract_value;
	private String currency;
	private int LGNumber;
	private int transactionId;
	public String getLGType() {
		return LGType;
	}
	public void setLGType(String lGType) {
		LGType = lGType;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectNumber() {
		return projectNumber;
	}
	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}
	public String getByanNumber() {
		return byanNumber;
	}
	public void setByanNumber(String byanNumber) {
		this.byanNumber = byanNumber;
	}
	public Date getZakatPeriodStart() {
		return zakatPeriodStart;
	}
	public void setZakatPeriodStart(Date zakatPeriodStart) {
		this.zakatPeriodStart = zakatPeriodStart;
	}
	public Date getZakatPeriodEndDate() {
		return zakatPeriodEndDate;
	}
	public void setZakatPeriodEndDate(Date zakatPeriodEndDate) {
		this.zakatPeriodEndDate = zakatPeriodEndDate;
	}
	public String getPurposeOfBbond() {
		return purposeOfBbond;
	}
	public void setPurposeOfBbond(String purposeOfBbond) {
		this.purposeOfBbond = purposeOfBbond;
	}
	public Date getLGValidity() {
		return LGValidity;
	}
	public void setLGValidity(Date lGValidity) {
		LGValidity = lGValidity;
	}
	public String getHijriDate() {
		return hijriDate;
	}
	public void setHijriDate(String hijriDate) {
		this.hijriDate = hijriDate;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getAmountText() {
		return amountText;
	}
	public void setAmountText(String amountText) {
		this.amountText = amountText;
	}
	public String getPercentageOfContract_value() {
		return percentageOfContract_value;
	}
	public void setPercentageOfContract_value(String percentageOfContract_value) {
		this.percentageOfContract_value = percentageOfContract_value;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public int getLGNumber() {
		return LGNumber;
	}
	public void setLGNumber(int lGNumber) {
		LGNumber = lGNumber;
	}
	public int getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
	
	
	
	

}
