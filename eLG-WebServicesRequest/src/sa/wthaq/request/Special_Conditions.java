package sa.wthaq.request;

public class Special_Conditions {
	
	private Boolean openEndedLG;
	private Boolean autoRenewalLG;
	private Boolean transferableLG;
	private Boolean assignableLG;
	//representativeInfo;
	private String actualApplicantarName;
	private String actualApplicantenName;
	private String PObox;
	private String postalCode;
	private String Address;
	private String contactNumber;
	private String faxNumber;
	private String emailAddress;
	public Boolean getOpenEndedLG() {
		return openEndedLG;
	}
	public void setOpenEndedLG(Boolean openEndedLG) {
		this.openEndedLG = openEndedLG;
	}
	public Boolean getAutoRenewalLG() {
		return autoRenewalLG;
	}
	public void setAutoRenewalLG(Boolean autoRenewalLG) {
		this.autoRenewalLG = autoRenewalLG;
	}
	public Boolean getTransferableLG() {
		return transferableLG;
	}
	public void setTransferableLG(Boolean transferableLG) {
		this.transferableLG = transferableLG;
	}
	public Boolean getAssignableLG() {
		return assignableLG;
	}
	public void setAssignableLG(Boolean assignableLG) {
		this.assignableLG = assignableLG;
	}
	public String getActualApplicantarName() {
		return actualApplicantarName;
	}
	public void setActualApplicantarName(String actualApplicantarName) {
		this.actualApplicantarName = actualApplicantarName;
	}
	public String getActualApplicantenName() {
		return actualApplicantenName;
	}
	public void setActualApplicantenName(String actualApplicantenName) {
		this.actualApplicantenName = actualApplicantenName;
	}
	public String getPObox() {
		return PObox;
	}
	public void setPObox(String pObox) {
		PObox = pObox;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getFaxNumber() {
		return faxNumber;
	}
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
 
}
