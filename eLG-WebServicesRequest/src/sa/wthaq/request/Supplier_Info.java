package sa.wthaq.request;

public class Supplier_Info {
	
	private String supplierName;
	private String supplierNameAr;
	private String applicantCR;
	private String applicantIBAN;
	private String PObox;
	private String postalcode;
	private String Address;
	private String contactNumber;
	private String faxNumber;
	private String emailAddress;
	
	public String getApplicantCR() {
		return applicantCR;
	}
	public void setApplicantCR(String applicantCR) {
		this.applicantCR = applicantCR;
	}
	public String getApplicantIBAN() {
		return applicantIBAN;
	}
	public void setApplicantIBAN(String applicantIBAN) {
		this.applicantIBAN = applicantIBAN;
	}
	public String getPObox() {
		return PObox;
	}
	public void setPObox(String pObox) {
		PObox = pObox;
	}
	public String getPostalcode() {
		return postalcode;
	}
	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getFaxNumber() {
		return faxNumber;
	}
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getSupplierNameAr() {
		return supplierNameAr;
	}
	public void setSupplierNameAr(String supplierNameAr) {
		this.supplierNameAr = supplierNameAr;
	}
	
	
	

}
