package sa.wthaq.webservices;

import java.util.Date;
import java.util.Random;

import sa.wthaq.request.MessageFromBank;
import sa.wthaq.request.MessageToBank;
import sa.wthaq.request.BankInfo;
import sa.wthaq.request.Beneficiary_Info;
import sa.wthaq.request.LG_Info;
import sa.wthaq.request.LgReturnInfo;
import sa.wthaq.request.ProfileAdmin;
import sa.wthaq.request.Special_Conditions;
import sa.wthaq.request.Supplier_Info;
import sa.wthaq.request.UserDetail;

public class ExternalSoapRequestImpl {
	
//mciData
	public UserDetail getRequestData(String crn,String mno) {
		UserDetail usdetail = new UserDetail();
		if(crn != null) {
		usdetail.setCrNumber(crn);
		}
		if(mno != null) {
		usdetail.setContactNo(mno);
		}
	usdetail.setAddress1("kolkata");
	usdetail.setAddress2("belgachia");
	usdetail.setEmailAddress("pomki@hghg.com");
	usdetail.setEntityName("demo entity"+new Random().nextInt());
	System.out.println("usdetail.getCrNumber()"+usdetail.getCrNumber());
		// UserDetail usrdetail = response from MCI check;
	System.out.println("usdetail"+usdetail.getCrNumber());
		return usdetail;
	}
	
//send admin details to cc	
	public String verificationAdminDetails(ProfileAdmin ad) {
		String msg = "Request Received";
		//ProfileAdmin admin = new ProfileAdmin();
		if(ad != null) {
		//admin.setRep_Id(ad.getRep_Id());
			System.out.println("ad.getRep_Id()"+ad.getRep_Id());
		//admin.setRep_name(ad.getRep_name());
			System.out.println("ad.getRep_name()"+ad.getRep_name());
		//admin.setRep_name_ar(ad.getRep_name_ar());
		System.out.println("ad.getRep_name_ar()"+ad.getRep_name_ar());
		//admin.setRep_email(ad.getRep_email());
		System.out.println("ad.getRep_email()"+ad.getRep_email());
		//admin.setRep_mobile_No(ad.getRep_mobile_No());
		System.out.println("ad.getRep_mobile_No()"+ad.getRep_mobile_No());
		//admin.setUser_Id(ad.getUser_Id());
		System.out.println("ad.getUser_Id()"+ad.getUser_Id());
		//admin.setRep_msg(msg);
		//String detadmin = Chambers  provided api response
		msg = "CC Verification Request Received";
		}else {
			msg = "CC Verification Request Error";
		}
		
		return msg;
	}
	
	//after verify details received from cc
	public ProfileAdmin afterCCVerified(String userId) {
		//String msg = "Verified";
		ProfileAdmin admin = new ProfileAdmin();
		if(userId != null) {
			admin.setUser_Id(userId);
			admin.setRep_status(true);
			//admin.setRep_Id(rep_Id);
			admin.setRep_msg("Verified");
		}else {
			admin.setRep_status(false);
			admin.setRep_msg("Not Verified");
		}
		return admin;
		
	}
	
	//send user iban to verify from bank
	public String verifyIbanDetail(String bi,String bankName,String userId) {
		String msg = "IBan Request Not Received";
		if(bi != "" && bankName !="" && userId!="") {
			
			//String bank = Bank  provided api response
			msg="IBan Request Received";
			System.out.println("bi not null"+bi);
		}
		
		
		return msg;
	}
	
	public String ForBeneficiaryverifyIbanDetail(String beneficiaryName,String ibanNo,String crNo,String userId) {
		String msg = "IBan Request Not Sent For Beneficiary";
		if(beneficiaryName != "" && ibanNo !="" && crNo !="" && userId!="") {
			
			//String bank = Bank  provided api response
			msg="IBan Request Sent For Beneficiary";
			System.out.println("bi not null"+ibanNo);
		}
		
		
		return msg;
	}
	
	
	public MessageFromBank getAcknowledgeFromBank(String userId) {
		MessageFromBank bank = new MessageFromBank();
		bank.setRequest_Id(userId);
		bank.setTimeData(new Date());
		bank.setStatus_code("200");
		bank.setStatus_msg("iban details received to verify");
		bank.setBankMessage_Id("2");
		return bank;
	}
	
	public String ForSupplierverifyIbanDetail(String supplierName,String ibanNo,String crNo,String userId) {
		String msg = "IBan Request Not Sent For Supplier";
		if(supplierName != "" && ibanNo !="" && crNo !="" && userId!="") {
			
			//String bank = Bank  provided api response
			msg="IBan Request Sent For Supplier";
			System.out.println("bi not null"+ibanNo);
		}
		
		
		return msg;
	}
	
	//getting userdetails after verification from bank
	public MessageFromBank getBankDetailsFromReq(String iban,String user_Id) {
		MessageFromBank info = new MessageFromBank();
		if(iban != "") {
			info.setRequest_Id(user_Id);
			info.setMessageToApplicant("Confirmed Iban details");
			info.setValid_flag("true");
			info.setTimeData(new Date());
			info.setBankMessage_Id("2");
			System.out.println("bi not null"+iban);
			System.out.println("inside Another"+iban);
		}else {
			info.setRequest_Id(user_Id);
			//info.setMessageToApplicant("Confirmed Iban details");
			info.setValid_flag("false");
			info.setTimeData(new Date());
			info.setBankMessage_Id("1");
			info.setMessageToApplicant("Rejected Iban details");
		}
		return info;
		
	}
	
	public BankInfo ForSuppliergetBankDetailsResponse(String iban) {
		BankInfo info = new BankInfo();
		if(iban != "") {
			info.setBank_code("UTI000022");
			info.setIban_no(iban);
			info.setBranch_name("Saltlake");
			
			info.setMsg("Verification Successful");
			System.out.println("bi not null"+iban);
			System.out.println("inside Another"+iban);
		}else {
			info.setMsg("Iban no not found");
		}
		return info;
		
	}
	
	public BankInfo ForBeneficiarygetBankDetailsResponse(String iban) {
		BankInfo info = new BankInfo();
		if(iban != "") {
			info.setBank_code("UTI000022");
			info.setIban_no(iban);
			info.setBranch_name("Saltlake");
			
			info.setMsg("Verification Successful");
			System.out.println("bi not null"+iban);
			System.out.println("inside Another"+iban);
		}else {
			info.setMsg("Iban no not found");
		}
		return info;
		
	}
	
	public MessageToBank sendAcknowledgeToBank(String userId) {
		MessageToBank bank = new MessageToBank();
		bank.setRequest_id(userId);
		bank.setTimeData(new Date());
		bank.setStatus_code("200");
		//bank.setStatus_msg("iban details received to verify");
		bank.setBankMessage_Id("2");
		return bank;
	}
	
	// Sending Lg details to bank
		
	
	public String sendIssueOfLGtoBankToVerify(String request_Id,Supplier_Info info,Beneficiary_Info info2,LG_Info lg_Info,Special_Conditions conditions) {
		String msg = "";
		if(request_Id != "") {
			msg="Lg Issue request sent Successfully.";
		}else {
			msg="Lg Issue request cannot be sent.";
		}
		
		return msg;
	}
	
	// Receiving Acknowledge message from bank
	//after they received our request
	
	public MessageFromBank getAcknowledgeFromBankAfterLgIssueRequest(String request_Id) {
		MessageFromBank bank = new MessageFromBank();
		bank.setRequest_Id(request_Id);
		bank.setTimeData(new Date());
		bank.setStatus_code("200");
		bank.setStatus_msg("Lg Issue details received to verify");
		bank.setBankMessage_Id("2");
		return bank;
	}
	
	
	//Receiving lg response
	//from bank
	
	public LgReturnInfo getLgDetailsFromBankAfterVerify(String user_Id,String request_Id) {
		LgReturnInfo info = new LgReturnInfo();
		Beneficiary_Info beneficiary_Info = new Beneficiary_Info();
		Supplier_Info info2 = new Supplier_Info();
		LG_Info lg_Info = new LG_Info();
		lg_Info.setTransactionId(new Random().nextInt());
		lg_Info.setLGNumber(new Random().nextInt());
		Special_Conditions conditions = new Special_Conditions();
		if(user_Id != "" && request_Id !="") {
			info.setBeneficiary_Info(beneficiary_Info);
			info.setInfo(lg_Info);
			info.setSupplier_Info(info2);
			info.setConditions(conditions);
			System.out.println("bi not null"+user_Id);
			System.out.println("inside Another"+user_Id);
		}else {
			//info.setRequest_Id(user_Id);
			
		}
		return info;
		
	}
	
	//After confirmation/rejection of 
	//lg issue request received from bank, 
	//acknowledge message send to bank
	
	public MessageFromBank sendAcknowledgeToBankAfterLgIssueConfirm(String request_Id,String user_Id) {
		MessageFromBank bank = new MessageFromBank();
		bank.setRequest_Id(request_Id);
		bank.setTimeData(new Date());
		bank.setStatus_code("200");
		bank.setStatus_msg("Lg Issue Verification details received");
		bank.setBankMessage_Id("LG"+new Random().nextInt());
		return bank;
	}
	
	
	

}
