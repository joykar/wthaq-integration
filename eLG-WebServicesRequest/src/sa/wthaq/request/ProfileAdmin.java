package sa.wthaq.request;

public class ProfileAdmin {
	
	private String rep_Id;
	private String rep_name;
	private String rep_name_ar;
	private String rep_email;
	private String rep_mobile_No;
	private String user_Id;
	private String rep_msg;
	private Boolean rep_status;
	
	public String getRep_Id() {
		return rep_Id;
	}
	public void setRep_Id(String rep_Id) {
		this.rep_Id = rep_Id;
	}
	public String getRep_name() {
		return rep_name;
	}
	public void setRep_name(String rep_name) {
		this.rep_name = rep_name;
	}
	public String getRep_name_ar() {
		return rep_name_ar;
	}
	public void setRep_name_ar(String rep_name_ar) {
		this.rep_name_ar = rep_name_ar;
	}
	public String getRep_email() {
		return rep_email;
	}
	public void setRep_email(String rep_email) {
		this.rep_email = rep_email;
	}
	public String getRep_mobile_No() {
		return rep_mobile_No;
	}
	public void setRep_mobile_No(String rep_mobile_No) {
		this.rep_mobile_No = rep_mobile_No;
	}
	public String getUser_Id() {
		return user_Id;
	}
	public void setUser_Id(String user_Id) {
		this.user_Id = user_Id;
	}
	public String getRep_msg() {
		return rep_msg;
	}
	public void setRep_msg(String rep_msg) {
		this.rep_msg = rep_msg;
	}
	public Boolean getRep_status() {
		return rep_status;
	}
	public void setRep_status(Boolean rep_status) {
		this.rep_status = rep_status;
	}
	
	
	
	

}
