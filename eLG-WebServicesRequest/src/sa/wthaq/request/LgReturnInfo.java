package sa.wthaq.request;

public class LgReturnInfo {
	
	private String request_Id;
	
	private LG_Info info;
	private Supplier_Info supplier_Info;
	private Beneficiary_Info beneficiary_Info;
	private Special_Conditions conditions;
	public LG_Info getInfo() {
		return info;
	}
	public void setInfo(LG_Info info) {
		this.info = info;
	}
	public Supplier_Info getSupplier_Info() {
		return supplier_Info;
	}
	public void setSupplier_Info(Supplier_Info supplier_Info) {
		this.supplier_Info = supplier_Info;
	}
	public Beneficiary_Info getBeneficiary_Info() {
		return beneficiary_Info;
	}
	public void setBeneficiary_Info(Beneficiary_Info beneficiary_Info) {
		this.beneficiary_Info = beneficiary_Info;
	}
	public Special_Conditions getConditions() {
		return conditions;
	}
	public void setConditions(Special_Conditions conditions) {
		this.conditions = conditions;
	}
	public String getRequest_Id() {
		return request_Id;
	}
	public void setRequest_Id(String request_Id) {
		this.request_Id = request_Id;
	}
	
	
	

}
