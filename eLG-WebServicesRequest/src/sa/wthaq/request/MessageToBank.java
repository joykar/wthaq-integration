package sa.wthaq.request;

import java.util.Date;

public class MessageToBank {
	
	private String request_Id;
	private String bankMessage_Id;
	private Date timeData;
	private String status_code;
	public String getRequest_id() {
		return request_Id;
	}
	public void setRequest_id(String request_id) {
		this.request_Id = request_id;
	}
	public String getBankMessage_Id() {
		return bankMessage_Id;
	}
	public void setBankMessage_Id(String bankMessage_Id) {
		this.bankMessage_Id = bankMessage_Id;
	}
	public Date getTimeData() {
		return timeData;
	}
	public void setTimeData(Date timeData) {
		this.timeData = timeData;
	}
	public String getStatus_code() {
		return status_code;
	}
	public void setStatus_code(String status_code) {
		this.status_code = status_code;
	}
	
	

}
