package sa.wthaq.request;

public class BankInfo {
	
	private String iban_no;
	private String bank_code;
	private String branch_name;
	private String msg; 
	private String userId;
	public String getIban_no() {
		return iban_no;
	}
	public void setIban_no(String iban_no) {
		this.iban_no = iban_no;
	}
	public String getBank_code() {
		return bank_code;
	}
	public void setBank_code(String bank_code) {
		this.bank_code = bank_code;
	}
	public String getBranch_name() {
		return branch_name;
	}
	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	

}
